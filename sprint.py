import os
import random

if __name__ == "__main__":
    adjective = random.choice(open(os.path.dirname(os.path.abspath(__file__)) + "/adjectives.txt", "r").readlines()).strip().capitalize()
    noun = random.choice(open(os.path.dirname(os.path.abspath(__file__)) + "/nouns.txt", "r").readlines()).strip().capitalize()

    if noun[-1] in ['a', 'e', 'y']:
        adjective = adjective[:-1] + noun[-1]

    print('%s %s' % (adjective, noun))
