# -*- coding: utf-8 -*-
from pyquery import PyQuery as pq
import urllib

BASE_URL = 'https://pl.wiktionary.org/w/index.php?title=Kategoria:Język_polski_-_przymiotniki&pagefrom=%s'
FIRST_WORD = 'aaronowy'

LST = []

def get_page(pagefrom):
    url = BASE_URL % pagefrom
    handler = pq(url=url)
    return [x.text for x in handler('.mw-category-group li a')]

LST = get_page(FIRST_WORD)

while True:
    word = LST[-1].encode('utf-8')

    print('getting page %s' % word)
    words = get_page(urllib.quote(word))

    if words:
        LST.extend(words)
    else:
        break

adjectives = open('adjectives.txt', 'w')

for word in LST:
    if word and len(word) <= 8:
        adjectives.write(word.encode('utf-8') + '\n')

adjectives.close()
