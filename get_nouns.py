# -*- coding: utf-8 -*-
from pyquery import PyQuery as pq
import urllib

BASE_URL = 'https://pl.wiktionary.org/w/index.php?title=Kategoria:Język_polski_-_rzeczowniki&pagefrom=%s'
FIRST_WORD = 'abstynent'

LST = []

def get_page(pagefrom):
    url = BASE_URL % pagefrom
    handler = pq(url=url)
    return [x.text for x in handler('.mw-category-group li a')]

LST = get_page(FIRST_WORD)

while True:
    word = LST[-1].encode('utf-8')

    if len(LST) > 10000:
        break

    print('getting page %s' % word)
    words = get_page(urllib.quote(word))

    if words:
        LST.extend(words)
    else:
        break

adjectives = open('nouns.txt', 'w')

for word in LST:
    if word and len(word) <= 10:
        adjectives.write(word.encode('utf-8') + '\n')
adjectives.close()
